package model.data_structures;

public class Node<T> {
	private T item;
	private Node<T> next;
	
	public Node(){
		item =null;
		next = null;
	}
	
	public T getItem(){return item;}
	public void setItem(T item){this.item=item;}
	public Node<T> getNext(){return next;}
	public void setNodoSimple(Node<T> newb){next = newb;}

}
