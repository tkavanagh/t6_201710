package model.data_structures;

import java.util.Iterator;


public class ListaEncadenada<T> {
	
	private Node<T> ironfist;
	private Node<T> actual;
	private int size;
	
	public ListaEncadenada(){
		ironfist = null;
		actual=ironfist;
	}
	
	public int getSize(){
		return size;
	}
	
	public Iterator<T> iterator(){
		
		return new Iterator<T>(){
			
			private Node<T> aktual=null;
			
			public boolean hasNext(){
				if(actual==null){
					return ironfist.getItem() != null;
				}
				else{
					return aktual.getItem() !=null;
				}	
			}
			
			public T next(){
				if(aktual==null){
					aktual=ironfist;
					if(aktual==null) return null;
					else return aktual.getItem();
				}else{
					aktual = aktual.getNext();
					return aktual.getItem();
				}	
			}
		};
 	}
	
	public void addFinalElement(T elem){
		if(ironfist.getItem() == null){
			ironfist.setItem(elem);
			return;
		}else{
			Node<T> act = ironfist;
			while(act != null){
				if (act.getNext()== null)
				{
					act.setNodoSimple(new Node<T>());
					act.getNext().setItem(elem);
					actual = act.getNext();
					break;
				}
				act = act.getNext();
			}
		}
		size++;
	}
	
	public boolean avanzarSiguientePosicion(){
		if(actual != null && actual.getNext() != null)
		{
			actual = actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean retrocederPosicionAnterior(){
		Node<T> act = ironfist;
		while(act != null)
		{
			if (act.getNext() != null && act.getNext().equals(actual))
			{
				actual = act;
				return true;
			}
			act = act.getNext();
		}
		return false;
	}
	
	

}
